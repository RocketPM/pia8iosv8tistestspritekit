//
//  PIA8Scene.swift
//  pia8iosv8tistest
//
//  Created by A on 2018-11-02.
//  Copyright © 2018 A. All rights reserved.
//

import UIKit
import SpriteKit

class PIA8Scene: SKScene {
    
    var greenBox : SKNode?
    
    override func sceneDidLoad() {
        // Get label node from scene and store it for use later
        self.greenBox = self.childNode(withName: "//fancysquare") as? SKNode
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print ("TRYCK PÅ SKÄRMEN")
    
    if let theTouch = touches.first
    {
      let touchPlace = theTouch.location(in: self)
        print(touchPlace)
        
        var boxMovieStuff = SKAction.sequence([
            SKAction.fadeAlpha(to: 0.5, duration: 1),
            SKAction.move(to: touchPlace, duration: 1),
            SKAction.fadeAlpha(to: 1, duration: 1)])
        
        greenBox?.run(boxMovieStuff)
    }
    
    }

}
